import React from 'react';
import './App.css';
//import Header from './Components/Header';
//import HeaderImage from './Components/HeaderImage';
import Home from './home/components/Home';
import Movie from './movie/components/Movie';
import Comingsoon from './comingsoon/components/Comingsoon';
import Signup from './signup/components/Signup';
import Yourvisit from './yourvisit/components/Yourvisit'
import Booktickets from './booktickets/components/Booktickets'
import About from './about/components/About';
import Header from './layout/components/Header';
import { BrowserRouter, Route, Switch } from "react-router-dom";

function App() {
    return (
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Home}></Route>
          <Route exact path="/movie" component={Movie}></Route>
          <Route exact path="/comingsoon" component={Comingsoon}></Route>
          <Route exact path="/signup" component={Signup}></Route> 
          <Route exact path="/yourvisit" component={Yourvisit}></Route> 
          <Route exact path="/booktickets" component={Booktickets}></Route>
          <Route exact path="/about" component={About}></Route>   

    
        </Switch>
      </BrowserRouter>
    );
};


export default App;
