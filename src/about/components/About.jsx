import React from 'react';

const About = () => {
    return (
        <div>
            <h1>About Page</h1>
            <h1> Cinema Opening Hours</h1>
            <h1> 11:00 - 23:00 Mon - Sun</h1>
        </div>
    );
};

export default About;
