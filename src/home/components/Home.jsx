import React from 'react';
import HomeContent from './../../layout/components/HomeContent';

const Home = () => {
    return (
        <div>
            <h1>Home Page</h1>
            <HomeContent />
        </div>
    );
};

export default Home;