import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav } from 'react-bootstrap';
import HeaderImage from './HeaderImage';

const Header = () => {
    return (
        <div>
            <Navbar bg="dark" variant="dark">
                <Navbar.Brand>QA Cinemas</Navbar.Brand>
                <Nav className="mr-auto">
                    <Nav.Link href=""><Link to="/"> Home :</Link></Nav.Link>
                    <Nav.Link href=""><Link to="/movie"> Whats On :</Link></Nav.Link>
                    <Nav.Link href=""><Link to="/comingsoon"> Coming Soon :</Link></Nav.Link>
                    <Nav.Link href=""><Link to="/signup"> Sign Up :</Link></Nav.Link>
                    <Nav.Link href=""><Link to="/yourvisit"> Your Visit :</Link></Nav.Link>
                    <Nav.Link href=""><Link to="/booktickets"> Book Tickets:</Link></Nav.Link>
                    <Nav.Link href=""><Link to="/about"> About :</Link></Nav.Link>
                </Nav>
            </Navbar>
            <HeaderImage />
        </div>
    );
};

export default Header;