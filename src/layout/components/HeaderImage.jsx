import React from 'react';
import explosion from './../../images/bang.png'; // gives image path


const HeaderImage = () => {
    return (
        <img src={explosion} alt="kaboom" width="1150" height="160"></img>  
    );
}

export default HeaderImage;
