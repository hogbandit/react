import React from "react";
import SiteIntro from './SiteIntro';
import NowShowing from './NowShowing';


const HomeContent = () => {
    return (
        <div>
            <SiteIntro />
            <NowShowing />
        </div>
    );
};

export default HomeContent;