import React from 'react';
import terminator from './../../images/terminator.png'; // gives image path

const ImageSection = () => {
 return (
    <img src={terminator} alt="terminator" width="300" height="200"></img>
 );
}

export default ImageSection;