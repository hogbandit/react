import React from 'react';
import NowShowingCard from './NowShowingCard'


const NowShowing = () => {
    return (
        <div>
            <div class="row">
            <div class="column">
                <NowShowingCard />
            </div>
            <div class="column">
                <NowShowingCard />
            </div>
            <div class="column">
                <NowShowingCard />
            </div>
            </div>
            <div class="row">
            <div class="column">
                <NowShowingCard />
            </div>
            <div class="column">
                <NowShowingCard />
            </div>
            <div class="column">
                <NowShowingCard />
            </div>
            </div>
        </div>
    );
}

export default NowShowing;
