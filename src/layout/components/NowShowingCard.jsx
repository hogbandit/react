import React from 'react';
import yoda from './../../images/yoda.png'; // gives image path

const NowShowingCard = () => {
    return (
        <div>
            <img src={yoda} alt="yoda" width="300" height="160"></img>
        </div>
    );
}

export default NowShowingCard;