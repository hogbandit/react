import React from 'react';
import TextSection from './TextSection';
import ImageSection from './ImageSection';
const SiteIntro = () => {
    return (
        <div>
            <div class="row">
                <div class="column">
                    <ImageSection />
                </div>
                <div class="column">
                    <TextSection />
                </div>
                <div class="column">
                    <TextSection />
                </div>
            </div>
            <div class="row">
                <div class="column">
                    <ImageSection />
                </div>
                <div class="column">
                    <TextSection />
            </div>
            </div>
        </div>
    );
};

export default SiteIntro;
