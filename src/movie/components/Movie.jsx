import React, { Component } from 'react'
import { getMovies } from '../service/Movie.service'
import MovieCard from './MovieCard';

class Movie extends Component {

    constructor(props) {
        super(props);
        this.state = {
            movies: getMovies()
        }
    }

    render() {
        let movieCards = this.state.movies.map((movie) => {
            return (
                <MovieCard
                    key={movie.imdbID}
                    Title={movie.Title}
                    Poster={movie.Poster}
                    ShowTimes={movie.ShowTimes}
                    Plot={movie.Plot}
                />
            )
        })
        return (
            <div>
                {movieCards}
            </div>
        ) 
    }
}

export default Movie;