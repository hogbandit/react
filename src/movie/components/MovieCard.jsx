import React, { Component } from 'react'
import { Card, Container, Row, Col, Table } from 'react-bootstrap'
import '../styles/MovieCard.css'

class MovieCard extends Component {

    constructor(props) {
        super(props)
        this.state = {
            showMoreInformation: false
        }
        this.toggleMoreInformation = this.toggleMoreInformation.bind(this)
    }

    toggleMoreInformation() {
        this.setState({
            showMoreInformation: !this.state.showMoreInformation
        })
    }

    render() {
        let showTimes = this.props.ShowTimes.map((showTime) => {
            return (
                <tr><td>{showTime}</td></tr>
            )
        })
        return (
            <Card>
                <Card.Body>      
                    <Container fluid="sm">
                        <Card.Title>{this.props.Title}</Card.Title>
                        <Row>
                            <Col>
                                <img alt={this.props.Title} src={this.props.Poster}/>
                            </Col>
                            <Col>
                                <Card.Text>
                                    <div>
                                        <h4>Show Times</h4>
                                        <Table>
                                            <tbody>
                                                { showTimes }
                                            </tbody>
                                        </Table>
                                    </div>
                                </Card.Text>
                            </Col>
                        </Row>
                        <Card.Link onClick={this.toggleMoreInformation}>More Information</Card.Link>
                        <Card.Text>
                            { 
                                this.state.showMoreInformation ?
                                <Card.Text>
                                    <h4>Plot</h4>
                                    <p>{this.props.Plot}</p>
                                </Card.Text>: null
                            }
                        </Card.Text>
                    </Container>
                </Card.Body>
            </Card>
        )
    }
}

export default MovieCard;