export const getMovies = (movieId) => {
    return [
        {
            imdbID: "tt3896198",
            Title: "Guardians of the Galaxy Vol. 2",
            Poster: "https://m.media-amazon.com/images/M/MV5BNjM0NTc0NzItM2FlYS00YzEwLWE0YmUtNTA2ZWIzODc2OTgxXkEyXkFqcGdeQXVyNTgwNzIyNzg@._V1_SX300.jpg",
            ShowTimes: [
                "11:00",
                "12:30",
                "15:00",
                "16:30"
            ],
            Plot: "The Guardians struggle to keep together as a team while dealing with their personal family issues, notably Star-Lord's encounter with his father the ambitious celestial being Ego."
        },
        {
            imdbID: "tt0372784",
            Title: "Batman Begins",
            Poster: "https://m.media-amazon.com/images/M/MV5BOTY4YjI2N2MtYmFlMC00ZjcyLTg3YjEtMDQyM2ZjYzQ5YWFkXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
            ShowTimes: [
                "11:00",
                "12:30",
                "15:00",
                "16:30"
            ],
            Plot: "After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from corruption."
        },
        {
            imdbID: "tt0126029",
            Title: "Shrek",
            Poster: "https://m.media-amazon.com/images/M/MV5BOGZhM2FhNTItODAzNi00YjA0LWEyN2UtNjJlYWQzYzU1MDg5L2ltYWdlL2ltYWdlXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg",
            ShowTimes: [
                "11:00",
                "12:30",
                "15:00",
                "16:30"
            ],
            Plot: "A mean lord exiles fairytale creatures to the swamp of a grumpy ogre, who must go on a quest and rescue a princess for the lord in order to get his land back."
        }
    ]
}