import React, { useState } from 'react';

const SignUpUrl = `http://localhost:4000/members`;

const Signup = () => {
        
    const [firstname, setFirstName] = useState(``);
    const [surname, setSurName] = useState(``);
    const [email, setEmail] = useState(``);
    const [phone, setPhone] = useState(``);
    const [dateofbirth, setDateOfBirth] = useState(``);
    const [gender, setGender] = useState(``);   


    const resetStates = () => {
        setFirstName(``);
        setSurName(``);
        setEmail(``);
        setPhone(``);
        setDateOfBirth(``);
        setGender(``);
    }

    const submitForm = async event => {
        event.preventDefault();
        // Remember to validate values somewhere!
        const body = JSON.stringify({ firstname, surname, email, phone, dateofbirth, gender });
        const requestOptions = {
            method: `POST`,
            headers: { 'Content-Type': 'application/json' },
            body
        };
        const response = await fetch(SignUpUrl, requestOptions);
        const result = await response.json();
        // Should do error checking and perhaps display success 
        // message to user
        console.log(result);
        resetStates();
    }

    return (
        <>
            <h1>Join The Club</h1>
            <p/>
            <h3>Add Your Details:</h3>
            <form id="table-wrapper" onSubmit={submitForm}>
                <fieldset>
                    <label htmlFor="firstname">First Name:&nbsp;</label>
                    <input type="text" name="firstname" value={firstname} onChange={event => setFirstName(event.target.value)} />
                </fieldset>
                <fieldset>
                    <label htmlFor="surname">Surname:&nbsp;</label>
                    <input type="text" surname="surname" value={surname} onChange={event => setSurName(event.target.value)} />
                </fieldset>
                <fieldset>
                    <label htmlFor="email">Email:&nbsp;</label>
                    <input type="text" email="email" value={email} onChange={event => setEmail(event.target.value)} />
                </fieldset>                              
                <fieldset>
                    <label htmlFor="gender">Gender:&nbsp;</label>
                    <select name="gender" value={gender} onChange={event => setGender(event.target.value)}>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                    </select>
                </fieldset>
                <fieldset>
                    <input type="submit" value="Book Now" />
                </fieldset>
            </form>
        </>
    );
};

export default Signup;
